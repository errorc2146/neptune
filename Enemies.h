#ifndef ENEMYCLASS_H
#define	ENEMYCLASS_H

#include "Globals.h"
#include "FloatText.h"
#include "Sprite.h"
#include "Pickups.h"
#include "EnemyProjectiles.h"

enum EnemyType 
{
    Grunt, Clapper, Spinner, Weird, 
    Bird, Dispensor, Spammer, Angst,
    Asteroid
};

class Enemy
{
private:
    float X,Y,W,H;
    float XVel, YVel;
    int Health;
    int DamageFrames;
    int Frame, Frametime;
    int ShotCooldown;
    int Spare1, Spare2, Spare3;
    Sprite CurrentSprite;
    EnemyType Type;
    
public:
    Enemy(int x, int y, EnemyType TypeEnum, int Spare);
    SDL_Rect GetCollisionRect(); 
    bool IsDead();
    bool IsColliding(SDL_Rect Rect);
    void Move(float PlayerX, float PlayerY);
    void Damage(int DamageDealt);
    void Render();
};

extern std::vector <Enemy> EnemyVector;

void DoEnemies(float PlayerX, float PlayerY);
void SpawnEnemy(float X, float Y, EnemyType Type, int Spare = 0);
#endif

