#include "LevelPatterns.h"

EnemyType CurrentEnemy;
EnemyPattern CurrentEnemyPattern;

bool Finished = true;

int *CurrentLevel;
int NumberLeft;
int Pause;
int LevelIncrement;
int NumberOfLines;
int Ticks;

//Enemy, pattern, number, inter-delay, post-delay
int LevelZero[] =
{
    Grunt, Line, 5, 2200, 10000,
    Clapper, Dart, 3, 2000, 8000,
    Bird, Single, 5, 1500, 10000,
    Grunt, Dart, 3, 1500, 6000,
    Weird, Single, 15, 400, 30000,
    Clapper, VertLine, 3, 1500, 5000,
    Spinner, Line, 4, 900, 35000
};

int LevelOne[] =
{
    Bird, Single, 10, 700, 14000,
    Spammer, VertLine, 4, 6000, 42000,
    Weird, Single, 35, 450, 20000,
    Dispensor, Single, 10, 800, 30000,
    Asteroid, Single, 40, 400, 50000,
    
};

int *LevelArray[] = 
{
    LevelZero,
    LevelOne
};

int LevelLines[] = {7, 4};

void RunLevel(int Level[], int NumLines)
{
    CurrentLevel = Level;
    NumberOfLines = NumLines;
    LevelIncrement = 0;
    
    CurrentEnemy = (EnemyType)Level[0];
    CurrentEnemyPattern = (EnemyPattern)Level[1];
    NumberLeft = Level[2];
    Pause = Level[3];
    
    Finished = false;
    Ticks = SDL_GetTicks();
}

void CheckLevel()
{
    if (!Finished && SDL_GetTicks() - Ticks >= Pause)
    {
        RunPattern(CurrentEnemyPattern, CurrentEnemy);
        Ticks = SDL_GetTicks();
        Pause = CurrentLevel[LevelIncrement + 3];
        
        if(--NumberLeft == 0)
        {
            Pause = CurrentLevel[LevelIncrement + 4];
            LevelIncrement += 5;
            
            if (LevelIncrement == NumberOfLines * 5) Finished = true;
            
            else
            {
                CurrentEnemy = (EnemyType)CurrentLevel[LevelIncrement];
                CurrentEnemyPattern = (EnemyPattern)CurrentLevel[LevelIncrement + 1];
                NumberLeft = CurrentLevel[LevelIncrement + 2];
                Pause = CurrentLevel[LevelIncrement + 3];
            }
        }
    }
}