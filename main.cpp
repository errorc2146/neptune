#include "Globals.h"
#include "Sprite.h"
#include "Game.h"

int main(int argc, char** argv) 
{   
    if (!Init()) return 1;
    
    SDL_Surface *Text = TTF_RenderText_Blended(BigFont, "Neptune", White);
    ApplySurface((SCREENWIDTH - Text->w) / 2, 200, Text);
    SDL_FreeSurface(Text);

    Text = TTF_RenderText_Blended(MedFont, "Press any key", White);
    ApplySurface((SCREENWIDTH - Text->w) / 2, SCREENHEIGHT - 200, Text);
    SDL_FreeSurface(Text);
    
    SDL_Flip(Screen);
    
    bool Done = false;
    
    SDL_Event Event;
    while(!Done)
    {
        while (SDL_PollEvent(&Event))
        {
            if (Event.type == SDL_KEYDOWN) Done = true;
        }
    }    
    
    Game();
    SDL_Quit();
    return 0;
}

