#include "FloatText.h"

std::vector <FloatingText> FloatingTextVector;

FloatingText::FloatingText(int x, int y, std::string Input, SDL_Color IColour, int Time)
{
    X = x;
    Y = y;
    Text = Input;
    Colour = IColour;
    Frames = Time;
}

void FloatingText::Do()
{
    SDL_Surface *ApplyMe = TTF_RenderText_Solid(RealSmallFont, Text.c_str(), Colour);
    ApplySurface(X, Y, ApplyMe);
    SDL_FreeSurface(ApplyMe);
    
    Frames--;
}

bool FloatingText::IsFinished()
{
    return (Frames == 0);
}

void FloatSomeText(int X, int Y, std::string Input, SDL_Color Colour, int Frames)
{
    FloatingText PushMe(X, Y, Input, Colour, Frames);
    FloatingTextVector.push_back(PushMe);
}

void DoText()
{
    for (int i = 0; i < FloatingTextVector.size(); i++)
        FloatingTextVector.at(i).Do();

    FloatingTextVector.erase(
        std::remove_if(
        FloatingTextVector.begin(),
        FloatingTextVector.end(),
        std::mem_fun_ref((&FloatingText::IsFinished))),
        FloatingTextVector.end());
}