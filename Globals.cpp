#include "Globals.h"

SDL_Surface *Screen = NULL;

SDL_Colour White {255, 255, 255};

SDL_Rect ScreenRect {0, 0, SCREENWIDTH, SCREENHEIGHT};

TTF_Font *BigFont = NULL;
TTF_Font *MedFont = NULL;
TTF_Font *SmallFont = NULL;
TTF_Font *RealSmallFont = NULL;

std::stringstream SpareStream;

int Score = 0;
int Lives = 3;
int Power = 0;

bool Init()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
    {
        fprintf(stderr, "Couldn't initiate SDL: %s\n", SDL_GetError());
        return false;
    }
    
    if (TTF_Init() == -1)
    {
        fprintf(stderr, "Couldn't initiate SDL_ttf: %s\n", TTF_GetError());
        return false;
    }
    
    if(Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1) 
        return false;
    
    Screen = SDL_SetVideoMode(SCREENWIDTH, SCREENHEIGHT, 32, SDL_SWSURFACE | SDL_NOFRAME);
    if (Screen == NULL)
    {
        fprintf(stderr, "Couldn't set video mode: %s\n", SDL_GetError());
        return false;
    }
    
    BigFont = TTF_OpenFont("prstart.ttf", 128);
    MedFont = TTF_OpenFont("prstart.ttf", 48);
    SmallFont = TTF_OpenFont("prstart.ttf", 24);
    RealSmallFont = TTF_OpenFont("prstart.ttf", 12);
    
    srand(time(NULL));
    
    return true;
}

bool MakeBetween(float Min, float *Value, float Max)
{
    if (*Value < Min)
    {
        *Value = Min;
        return true;
    }
    
    else if (*Value > Max) 
    {
        *Value = Max;
        return true;
    }
    
    return false;
}

bool InBetween(float Min, float Value, float Max)
{
    return (Value > Min && Value < Max);
}

bool IsIntersecting(SDL_Rect Rect1, SDL_Rect Rect2)
{
    return !(Rect1.x > Rect2.x + Rect2.w || Rect1.x + Rect1.w < Rect2.x 
            || Rect1.y > Rect2.y + Rect2.h || Rect1.y + Rect1.h < Rect2.y);
}

void ApplySurface(int X, int Y, SDL_Surface *Source, SDL_Surface *Destination)
{
    SDL_Rect Position;
    Position.x = X;
    Position.y = Y;
    SDL_BlitSurface(Source, NULL, Destination, &Position);
}