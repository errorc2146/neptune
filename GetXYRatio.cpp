#include"GetXYRatio.h"
#define CONVERSION (3.1418 / 180)

void GetXYRatio(float *x, float *y, float Angle, float Distance)
{
	if (Angle > 360) Angle -= 360;

	if (Angle > 270)
	{
		Angle -= 270;
		*x = -1 * Distance * cos(Angle * CONVERSION);
		*y = -1 * Distance * sin(Angle * CONVERSION);
	}
	else if (Angle > 180)
	{
		Angle = 270 - Angle;
		*x = -1 * Distance * cos(Angle * CONVERSION);
		*y =  Distance * sin(Angle * CONVERSION);
	}
	else if (Angle > 90)
	{
		Angle -= 90;
		*x = Distance * cos(Angle * CONVERSION);
		*y =  Distance * sin(Angle * CONVERSION);
	}
	else
	{
		Angle = 90 - Angle;
		*x = Distance * cos(Angle * CONVERSION);
		*y = -1 * Distance * sin(Angle * CONVERSION);
	}
}
