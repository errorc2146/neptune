#include "EnemyPatterns.h"

std::vector <RunningPattern> PatternVector;

RunningPattern::RunningPattern(EnemyPattern TypeEnum, EnemyType EnemyEnum, float x,  float y, int Parameter, int Num, int Del)
{
    Type = TypeEnum;
    SpawnMe = EnemyEnum;
    Number = Num;
    Delay = Del;
    X = x;
    Y = y;
    Time = 0;
    Spare = Parameter;
}

void RunningPattern::Run()
{
    Time++;
    
    if (Time == Delay)
    {
        Time = 0;
        
        switch (Type)
        {
            case Line:
                SpawnEnemy(X, Y, SpawnMe);
                Number--;
                break;

            case Dart:
                if (Spare == 0) 
                {
                    SpawnEnemy(X, Y, SpawnMe);
                    Number--;
                }
                
                else
                {
                    SpawnEnemy(X, Y + 50 * Spare, SpawnMe);
                    SpawnEnemy(X, Y - 50 * Spare, SpawnMe);
                    Number -= 2;
                }
                Spare++;
                break;
                
            case VertLine:
                for (int i = 0; i < Number; i++)
                    SpawnEnemy(X, Y + i * 75, SpawnMe);
                Number = 0;
                break;
                
            case Single:
                SpawnEnemy(X, Y, SpawnMe);
                Number = 0;
                break;
        }
    }
}

bool RunningPattern::IsFinished()
{
    return (Number == 0);
}

void RunPattern(EnemyPattern PatternEnum, EnemyType EnemyEnum, float X, float Y, int Spare, int Number, int Delay)
{   
    if (Number == 0)
    {
        switch (PatternEnum)
        {
            case Line:
                Number = 6;
                break;
                
            case Dart:
                Number = 7;
                break;
                
            case VertLine:
            case Single:
                Number = 6;
                break;
        }
    }
    
    if (Delay == 0)
    {
        switch (PatternEnum)
        {
            case Line:
            case Dart:
            case VertLine:
            case Single:
                Delay = 10;
                break;
        }
    }
    
    if (Y == 0)
    {
        switch (PatternEnum)
        {
            case Line:
            case Dart:
                Y = rand() % SCREENHEIGHT;
                break;
            case VertLine:
                Y = rand() % (SCREENHEIGHT / 2);
                break;
            case Single:
                Y = rand() % SCREENHEIGHT;
                break;
        }
    }
    
    if (X == 0) X = SCREENWIDTH;
    
    RunningPattern PushMe(PatternEnum, EnemyEnum, X, Y, Spare, Number, Delay);
    PatternVector.push_back(PushMe);
}

void CheckPatterns()
{
    for (int i = 0; i < PatternVector.size(); i++)
        PatternVector.at(i).Run();
    
    PatternVector.erase(
        std::remove_if(
        PatternVector.begin(),
        PatternVector.end(),
        std::mem_fun_ref((&RunningPattern::IsFinished))),
        PatternVector.end());
}