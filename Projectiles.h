#ifndef PROJECTILES_H
#define	PROJECTILES_H

#include "Globals.h"

enum ProjectileType {StdBullet};

class Projectile
{
private:
    ProjectileType TypeEnum;
    float X, Y, W, H;
    int XVel, YVel;
    int Damage;
    bool Dead;
    Uint32 Colour;
    
public:
    Projectile(float x, float y, ProjectileType Type);
    SDL_Rect GetCollisionRect();
    void Move();
    void Render();
    void Destroy();
    bool IsDead() const;
};

extern std::vector <Projectile> ProjectileVector;

void Shoot(float X, float Y, ProjectileType Type);
void DoProjectiles();
#endif

