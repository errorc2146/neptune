#ifndef LEVELPATTERNS_H
#define	LEVELPATTERNS_H
#include "Globals.h"
#include "EnemyPatterns.h"

#define LEVELONELINES 7
#define LEVELTWOLINES 4

extern EnemyType CurrentEnemy;
extern EnemyPattern CurrentEnemyPattern;

extern bool Finished;

extern int NumberLeft;
extern int Pause;

extern int *LevelArray[];
extern int LevelLines[];
extern int LevelZero[];
extern int LevelOne[];

void RunLevel(int Level[], int NumLines);
void CheckLevel();

#endif	

