#ifndef FLOATTEXT_H
#define	FLOATTEXT_H

#include "Globals.h"

class FloatingText
{
private:
    int X,Y;
    int Frames;
    SDL_Color Colour;
    std::string Text;
    
public:
    FloatingText(int x, int y, std::string Input, SDL_Color IColour, int Time);
    void Do();
    bool IsFinished();
};

extern std::vector <FloatingText> FloatingTextVector;

void FloatSomeText(int X, int Y, std::string Input, SDL_Color Colour = {255, 255, 255}, int Frames = 50);

void DoText();

#endif