#include "Sprite.h"

Sprite::Sprite(short unsigned int *Sprite, int Width, int Height,  Uint32 IColour, int FragSize)
{
    SpriteData = Sprite;
    W = Width;
    H = Height;
    FragmentSize = FragSize;
    Colour = IColour;
}

void Sprite::Update(short unsigned int *Sprite, int Width, int Height, Uint32 IColour, int FragSize)
{
    SpriteData = Sprite;
    
    if (Width != 0)
    {
        W = Width;
        H = Height;
    }
    
    if (IColour != 0xC0FFEE) Colour = IColour;
    
    FragmentSize = FragSize;
}

bool Sprite::IsColliding(int X, int Y, SDL_Rect Rect)
{
    float XOffset, YOffset;
    int XSpan, YSpan;

    if (Rect.x < X) 
    {
        XOffset = 0;
        XSpan = (Rect.x + Rect.w - X) / FragmentSize + 1;
    }

    else if (Rect.x + Rect.w > X + W * FragmentSize)
    {
        XOffset = (Rect.x - X) / FragmentSize;
        XSpan = (X + W * FragmentSize - Rect.x) / FragmentSize + 1;
    }

    else 
    {
        XOffset = (Rect.x - X) / FragmentSize;
        XSpan = Rect.w / FragmentSize + 1;
    }

    if (Rect.y < Y) 
    {
        YOffset = 0;
        YSpan = (Rect.y + Rect.h - Y) / FragmentSize + 1;
    }

    else if (Rect.y + Rect.h > Y + H * FragmentSize)
    {
        YOffset = (Rect.y - Y) / FragmentSize;
        YSpan = (Y + H * FragmentSize - Rect.y) / FragmentSize + 1;
    }

    else 
    {
        YOffset = (Rect.y - Y) / FragmentSize;
        YSpan = Rect.h / FragmentSize + 1;
    }

    for (int x = XOffset; x < XOffset + XSpan; x++)
    {
        for (int y = YOffset; y < YOffset + YSpan; y++)
        {
            if (y * W + x < W * H && SpriteData[y * W + x] == 1) return true;
        }
    }

    return false;
}

void Sprite::Render(int X, int Y, Uint32 OverrideColour, int OverrideFragSize)
{
    SDL_Rect Fragment;
    Fragment.w = FragmentSize;
    Fragment.h = FragmentSize;
    
    if (OverrideColour == 0xC0FFEE) OverrideColour = Colour;
    if (OverrideFragSize == 0) OverrideFragSize = FragmentSize;
    
    for (int i = 0; i < W * H; i++)
    {
        if(SpriteData[i])
        {
            Fragment.x = X + (i % W) * OverrideFragSize;
            Fragment.y = Y + (i / W) * OverrideFragSize;
            SDL_FillRect(Screen, &Fragment, OverrideColour);
        }
    }
}

void Sprite::Vaporise(int X, int Y, float XVel, float YVel)
{
    for (int i = 0; i < W; i++)
    {
        for (int j = 0; j < H; j++)
        {
            if (SpriteData[j * W + i])
            {
                SDL_Rect DebrisRect;
                DebrisRect.x = X + i * FragmentSize;
                DebrisRect.y = Y + j * FragmentSize;
                DebrisRect.w = FragmentSize;
                DebrisRect.h = FragmentSize;
                
                CreateDebris(DebrisRect, XVel + rand() % 5 - 2, YVel + rand() % 5 - 2, rand() % 150, Colour);
            }
        }
    }
}
