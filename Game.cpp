#include "Game.h"

void Game()
{
    Sprite Player(PlayerFrames, 8, 4, 0x00688B);
    
    float PlayerX = SCREENWIDTH / 8, PlayerY = SCREENHEIGHT / 2;
    float PlayerXVel = 0, PlayerYVel = 0;
    float Acceleration = 0.6;
    float TopSpeed = 8;
    
    int Ticks, SpawnTicks;
    int ShotCooldown = 0;
    int PlayerFragmentSize = 6;
    int InvunerabilityFrames = 0;
    int CurrentLevel = 0;
    
    bool Invunerable = false;
    bool GameOver = false;
    bool Alive = true;
    
    SDL_Surface *Ready = TTF_RenderText_Blended(MedFont, "Ready", White);
    SDL_Surface *ScoreSurface = NULL;
    
    Uint8 *KeyStates;
    
    for (int i = 0; i < 30; i++) //Add some stars
    {
        int Size = rand() % 2 + 3;
        SDL_Rect Star = {rand () % SCREENWIDTH, rand() % SCREENHEIGHT, Size, Size};
        CreateDebris(Star, -(rand() % 5 + 1), 0, 0xF0F0F0);
    }
    
    CurrentLevel++;
    
    while (!GameOver)
    {
        Ticks = SDL_GetTicks();

        if (Finished)
        {
            RunLevel(LevelArray[CurrentLevel], LevelLines[CurrentLevel]);
            
            SpareStream.str("");
            SpareStream << "Level " << CurrentLevel;
            FloatSomeText(PlayerX, PlayerY - 30, SpareStream.str().c_str());

            CurrentLevel++;
        }
        
        //Handling the keyboard
        SDL_PumpEvents();
        KeyStates = SDL_GetKeyState(NULL);
        
        if (KeyStates[SDLK_q])
            GameOver = true;
        
        else if (ShotCooldown == 0 && KeyStates[SDLK_x] && Alive)
        {
            switch (Power)
            {
                case 0:
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 2.5, StdBullet);
                    ShotCooldown = 20;
                    break;
                    
                case 1:
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 2, StdBullet);
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 3, StdBullet);
                    ShotCooldown = 15;
                    break;
                    
                case 2:
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 2, StdBullet);
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 2.5, StdBullet);
                    Shoot(PlayerX + PlayerFragmentSize * 6, PlayerY + PlayerFragmentSize * 3, StdBullet);
                    ShotCooldown = 10;
                    break;
            }
        }
        
        if (KeyStates[SDLK_RIGHT])
        {
            if (PlayerXVel < 0) PlayerXVel = 0;
            else if (PlayerXVel + Acceleration < TopSpeed) PlayerXVel += Acceleration;
            else PlayerXVel = TopSpeed;
        }
        
        else if (KeyStates[SDLK_LEFT])
        {
            if (PlayerXVel > 0) PlayerXVel = 0;
            else if (PlayerXVel - Acceleration > -TopSpeed) PlayerXVel -= Acceleration;
            else PlayerXVel = -TopSpeed;
        }
        
        else PlayerXVel /= 2;
        
        if (KeyStates[SDLK_DOWN])
        {
            if (PlayerYVel < 0) PlayerYVel = 0;
            else if (PlayerYVel + Acceleration < TopSpeed) PlayerYVel += Acceleration;
            else PlayerYVel = TopSpeed;
        }
        
        else if (KeyStates[SDLK_UP])
        {
            if (PlayerYVel > 0) PlayerYVel = 0;
            else if (PlayerYVel - Acceleration > -TopSpeed) PlayerYVel -= Acceleration;
            else PlayerYVel = -TopSpeed;
        }
        
        else PlayerYVel /= 2;
        
        PlayerX += PlayerXVel;
        PlayerY += PlayerYVel;
        
        if (MakeBetween(0, &PlayerX, SCREENWIDTH - PlayerFragmentSize * 8)) PlayerXVel = 0;
        if (MakeBetween(0, &PlayerY, SCREENHEIGHT - PlayerFragmentSize * 4)) PlayerYVel = 0;
        
        if (ShotCooldown != 0) ShotCooldown--;
        
        if (rand() % 10 == 4)
        {
            int Size = rand() % 2 + 3;
            SDL_Rect Star = {SCREENWIDTH - 1, rand() % SCREENHEIGHT, Size, Size};
            CreateDebris(Star, -(rand() % 5 + 1), 0, 0xF0F0F0);
        }
        
        if (ResolveCollisions({PlayerX, PlayerY, 8 * PlayerFragmentSize, 4 * PlayerFragmentSize}, Player) && Alive && !Invunerable)
        {
            SpawnTicks = SDL_GetTicks();
            Alive = false;
            Power = 0;
            SDL_Delay(250);
            Player.Vaporise(PlayerX, PlayerY, PlayerXVel, PlayerYVel);
        }
        
        SDL_FreeSurface(ScoreSurface);
        SpareStream.str("");
        SpareStream << std::setfill('0');
        SpareStream << std::setw(8);
        SpareStream << Score;
        ScoreSurface = TTF_RenderText_Blended(SmallFont, SpareStream.str().c_str(), White);
        CheckLevel();
        CheckPatterns();
        
        SDL_FillRect(Screen, &ScreenRect, 0x000000);
        DoDebris();
        DoPickups();
        DoEnemies(PlayerX, PlayerY);
        DoProjectiles();
        DoEnemyProjectiles();
        DoText();
        
        if (!Alive)
        {
            if (SDL_GetTicks() - SpawnTicks > 5000)
            {
                PlayerX = SCREENWIDTH / 8, PlayerY = SCREENHEIGHT / 2;
                PlayerXVel = 0, PlayerYVel = 0;
                Acceleration = 0.6;
                TopSpeed = 8;
                Lives--;
                Alive = true;
                
                Invunerable = true;
                InvunerabilityFrames = 180;
            }
            
            else if (SDL_GetTicks() - SpawnTicks > 2000)
            {
                if (Lives == -1) GameOver = true;
                ApplySurface((SCREENWIDTH - Ready->w) / 2, (SCREENHEIGHT - Ready->h) / 2, Ready);
            }
        }
        
        else if (Alive && Invunerable)
        {
            if (InvunerabilityFrames / 40 % 2 == 0) Player.Render(PlayerX, PlayerY);
            if (--InvunerabilityFrames == 0) Invunerable = false;
        }
        
        else Player.Render(PlayerX, PlayerY);
        
        ApplySurface((SCREENWIDTH - ScoreSurface->w) / 2, 30, ScoreSurface);
        for (int i = 0; i < Lives; i++)
            Player.Render(10 + i * 64, SCREENHEIGHT - 40, 0x00FF00, 4);
        
        SDL_Flip(Screen);
        
        Ticks = SDL_GetTicks() - Ticks;
        if (Ticks < 1000 / 60) SDL_Delay(1000 / 60 - Ticks);
    }
}