#include "Enemies.h"

#define CURRENTENEMY EnemyVector.at(i)

Enemy::Enemy(int x, int y, EnemyType TypeEnum, int Spare)
{
    X = x;
    Y = y;
    Type = TypeEnum;
    Frame = 0;
    Frametime = 0;
    DamageFrames = 0;
    ShotCooldown = 0;
    Spare1 = Spare, Spare2 = 0, Spare3 = 0;
    
    switch(Type)
    {
        case Grunt:
            W = 5 * 6;
            H = 5 * 6;
            YVel = 0;
            XVel = -6;
            Health = 10;
            CurrentSprite.Update(GruntFrames[0], W / 6, H / 6, 0xE50000);
            break;
            
        case Clapper:
            W = 6 * 6;
            H = 4 * 6;
            YVel = 0;
            XVel = -4;
            Health = 10;
            CurrentSprite.Update(ClapperFrames[0], W / 6, H / 6, 0x50FEE0);
            break;
            
        case Spinner:
            W = 6 * 6;
            H = 6 * 6;
            Health = 10;
            Spare1 = x;
            Spare2 = y;
            Spare3 = 0;
            XVel = -4;
            YVel = 0;
            CurrentSprite.Update(SpinnerFrames[0], W / 6, H / 6, 0x5EF0EF);
            break;
            
        case Weird:
            W = 7 * 6;
            H = 3 * 6;
            YVel = 0;
            XVel = -4;
            Health = 10;
            CurrentSprite.Update(WeirdFrames[0], W / 6, H / 6, 0xF0F0F0);
            break;
            
        case Bird:
            W = 5 * 6;
            H = 3 * 6;
            YVel = 0;
            XVel = -4;
            Health = 10;
            ShotCooldown = rand() % 150 + 1;
            CurrentSprite.Update(BirdFrames[0], W / 6, H / 6, 0xFF0F0F);
            break;
            
        case Dispensor:
            W = 5 * 6;
            H = 5 * 6;
            YVel = 0;
            XVel = 0;
            Spare1 = -1;
            Spare2 = -1;
            Health = 20;
            CurrentSprite.Update(DispensorFrames[0], W / 6, H / 6, 0xFEEF0F);
            break;
            
        case Spammer:
            W = 6 * 6;
            H = 6 * 6;
            Health = 10;
            XVel = -(rand() % 3 + 2);
            YVel = rand() % 11 - 5;
            ShotCooldown = rand() % 60 + 1;
            CurrentSprite.Update(SpammerFrames[0], W / 6, H / 6, 0x5EF0EF);
            break;
            
        case Angst:
            W = 7 * 6;
            H = 7 * 6;
            Health = 30;
            XVel = -2;
            YVel = rand() % 7 - 3;
            ShotCooldown = rand() % 30 + 1;
            Spare1 = rand() % 360;
            CurrentSprite.Update(AngstFrames[0], W / 6, H / 6, 0xAEF4E0);
            break;
            
        case Asteroid:
            Health = 20 + (2 - Spare) * 15;
            XVel = -(rand() % 3 + 1);
            YVel = rand() % 3 - 2;
            if (Spare == 0) 
            {
                W = 15 * 6; H = 15 * 6;
                CurrentSprite.Update(AsteroidBig, 15, 15, 0x545454);
            }
            else if (Spare == 1) 
            {
                W = 11 * 6; H = 11 * 6;
                CurrentSprite.Update(AsteroidMed, 11, 11, 0x545454);
            }
            else
            {
                W = 7 * 6; H = 7 * 6;
                CurrentSprite.Update(AsteroidSmall, 7, 7, 0x545454, 6);
            }
            break;
    }
}

SDL_Rect Enemy::GetCollisionRect()
{
    SDL_Rect ReturnMe = {X, Y, W, H};
    return ReturnMe;
}

bool Enemy::IsDead()
{
    return (Health <= 0 || !InBetween(-W, X, SCREENWIDTH + 100));
}

bool Enemy::IsColliding(SDL_Rect Rect)
{
    if (IsIntersecting(Rect,GetCollisionRect()) && CurrentSprite.IsColliding(X,Y,Rect))
        return true;
   return false;
}

void Enemy::Move(float PlayerX, float PlayerY)
{
    if (DamageFrames != 0) 
    {
        DamageFrames--;
        X += rand() % 6 - 3;
        Y += rand() % 6 - 3;
    }
    
    switch(Type)
    {
        case Grunt:
        case Clapper:
            X += XVel;
            break;
            
        case Spinner:
            Spare1 += XVel;
            Spare2 += YVel;
            Spare3 += 3;
            Spare3 %= 360;
            
            float XOffset, YOffset;
            GetXYRatio(&XOffset, &YOffset, Spare3, 50);
            X = Spare1 + XOffset;
            Y = Spare2 + YOffset;
            break;
            
        case Weird:
            XVel -= 0.03;
            if (PlayerY > Y) YVel += 0.07;
            else YVel -= 0.07;
            MakeBetween(-4, &YVel, 4);
            
            X += XVel;
            Y += YVel;
            break;
            
        case Bird:
            X += XVel;
            if (--ShotCooldown == 0)
            {
                CreateEnemyProjectile(X + 12, Y + H / 2, StdProj);
                ShotCooldown = 150;
            }
            break;
            
        case Dispensor:
            if (Spare1 == -1)
            {
                Spare1 = PlayerX + 400;
                if (Spare1 > SCREENWIDTH - W) Spare1 = SCREENWIDTH - W;
                Spare2 = PlayerY;
            }
            
            XVel = Spare1 - X;
            YVel = Spare2 - Y;
            
            if (XVel * XVel + YVel * YVel < 2500)
            {
                for (int i = 235 ; i <= 305; i += 35)
                    CreateEnemyProjectile(X + 12, Y + H / 2, Slow1, i);
                Spare1 = SCREENWIDTH + 300;
                Spare2 = rand() % SCREENHEIGHT;
            }
            
            XVel /= 20;
            YVel /= 20;
            
            X += XVel;
            Y += YVel;
            
            break;
            
        case Spammer:
            X += XVel;
            Y += YVel;
            if (!InBetween(0, Y, SCREENHEIGHT - H)) YVel *= -1;
            
            if (--ShotCooldown == 0)
            {
                CreateEnemyProjectile(X + 12, Y + H / 2, Slow1, rand() % 360);
                ShotCooldown = 60;
            }
            break;
            
        case Angst:
            X += XVel;
            Y += YVel;
            if (rand() % 60 == 30) YVel *= -1;
            
            if (--ShotCooldown == 0)
            {
                CreateEnemyProjectile(X + 12, Y + H / 2, Slow1, Spare1);
                ShotCooldown = 45;
                Spare1 += 5;
                Spare1 %= 360;
            }
            break;
            
        case Asteroid:
            X += XVel;
            Y += YVel;
            if (!InBetween(0, Y, SCREENHEIGHT - H)) YVel *= -1;
            break;
    }
}

int ScoreArray[9] = {50, 100, 50, 50, 75, 150, 75, 100, 100};
void Enemy::Damage(int DamageDealt)
{
    Health -= DamageDealt;
    DamageFrames = 20;
    
    if (Health <= 0)
    {
        if (Type == Asteroid && Spare1 != 2)
        {
            SpawnEnemy(X, Y, Asteroid, ++Spare1);
            SpawnEnemy(X, Y, Asteroid, Spare1);
            SpawnEnemy(X, Y, Asteroid, Spare1);
        }
        
        CurrentSprite.Vaporise(X,Y, XVel, YVel);
        Score += ScoreArray[(int)Type];
        FloatSomeText(X + W/2, Y + H/2, std::to_string(ScoreArray[(int)Type]).c_str());
        
        if (rand() % 20 == 3)
        {
            int RandomNumber = rand() % 100;
            
            if (RandomNumber > 90) CreatePickup(X, Y, Points3);
            else if (RandomNumber > 70) CreatePickup(X, Y, Points2);
            else if (RandomNumber > 20) CreatePickup(X, Y, Points1);
            else if (RandomNumber > 10) CreatePickup(X, Y, Pow);
            else CreatePickup(X, Y, Life);
        }
    }
}


int FrametimeArray[9] = {40, 100, 30, 10, 100, 5, 10, 60, 0};
void Enemy::Render()
{
    if (FrametimeArray[(int)Type] != 0)
    {
        Frametime++;

        if (Frametime == FrametimeArray[(int)Type])
        {
            Frametime = 0;
            Frame = 1 - Frame;

            switch (Type)
            {
                case Grunt:
                    CurrentSprite.Update(GruntFrames[Frame]);
                    break;

                case Clapper:
                    CurrentSprite.Update(ClapperFrames[Frame]);
                    break;

                case Spinner:
                    CurrentSprite.Update(SpinnerFrames[Frame]);
                    break;

                case Weird:
                    CurrentSprite.Update(WeirdFrames[Frame]);
                    break;

                case Bird:
                    CurrentSprite.Update(BirdFrames[Frame]);
                    break;

                case Dispensor:
                    CurrentSprite.Update(DispensorFrames[Frame]);
                    break;

                case Spammer:
                    CurrentSprite.Update(SpammerFrames[Frame]);
                    break;

                case Angst:
                    CurrentSprite.Update(AngstFrames[Frame]);
                    break;
            };
        }
    }
    CurrentSprite.Render(X,Y);
}

std::vector <Enemy> EnemyVector;

void DoEnemies(float PlayerX, float PlayerY)
{
    for (int i = 0; i < EnemyVector.size(); i++)
    {
        CURRENTENEMY.Move(PlayerX, PlayerY);
        CURRENTENEMY.Render();
    }
    
    EnemyVector.erase(
        std::remove_if(
        EnemyVector.begin(),
        EnemyVector.end(),
        std::mem_fun_ref((&Enemy::IsDead))),
        EnemyVector.end());
}

void SpawnEnemy(float X, float Y, EnemyType Type, int Spare)
{
    Enemy PushMe(X, Y, Type, Spare);
    EnemyVector.push_back(PushMe);
}
