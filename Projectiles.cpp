#include"Projectiles.h"

#define CURRENTPROJECTILE ProjectileVector.at(i)

std::vector <Projectile> ProjectileVector;

Projectile::Projectile(float x, float y, ProjectileType Type)
{
    X = x;
    Y = y;
    TypeEnum = Type;
    Dead = false;
    
    switch (TypeEnum)
    {
        case StdBullet:
            W = 30;
            H = 4;
            XVel = 20;
            YVel = 0;
            Damage = 10;
            Colour = 0xE5E500;
            break;
    };      
}

SDL_Rect Projectile::GetCollisionRect()
{
    SDL_Rect ReturnMe {X,Y,W,H};
    return ReturnMe;
}

void Projectile::Move()
{
    X += XVel;
    Y += YVel;
}

void Projectile::Render()
{
    SDL_Rect RenderRect = GetCollisionRect();
    SDL_FillRect (Screen, &RenderRect, Colour);
}

void Projectile::Destroy()
{
    Dead = true;
}

bool Projectile::IsDead() const
{
    return Dead; 
}

void Shoot(float X, float Y, ProjectileType Type)
{
    Projectile PushMe(X,Y,Type);
    ProjectileVector.push_back(PushMe);
}

void DoProjectiles()
{
    for (int i = 0; i < ProjectileVector.size(); i++)
    {
        CURRENTPROJECTILE.Move();
        CURRENTPROJECTILE.Render();
        
        if (!IsIntersecting(CURRENTPROJECTILE.GetCollisionRect(), ScreenRect))
            CURRENTPROJECTILE.Destroy();
    }
    
        
    ProjectileVector.erase(
        std::remove_if(
        ProjectileVector.begin(),
        ProjectileVector.end(),
        std::mem_fun_ref((&Projectile::IsDead))),
        ProjectileVector.end());
}