#include "Pickups.h"

std::vector <Pickup> PickupVector;

Pickup::Pickup(float x, float y, PickupType TypeEnum)
{
    X = x; Y = y;
    XVel = -(rand() % 3 + 1);
    YVel = rand() % 5 - 2;
    Type = TypeEnum;
    CurrentSprite.Update(PickupSpriteArray[(int)Type], 7, 7, 0x00FF00, 3);
}

SDL_Rect Pickup::GetCollisionRect()
{
    return {X, Y, 21, 21};
}

void Pickup::Do()
{
    X += XVel;
    Y += YVel;
    if (!InBetween(0, Y, SCREENHEIGHT - 21)) YVel *= -1;
    
    CurrentSprite.Render(X,Y);
}

void Pickup::Destroy()
{
    switch(Type)
    {
        case Life:
            Lives++;
            FloatSomeText(X, Y, "+1", {32, 255, 32});
            break;

        case Points1:
            Score += 500;
            FloatSomeText(X, Y, "500");
            break;

        case Points2:
            Score += 1000;
            FloatSomeText(X, Y, "1000");
            break;

        case Points3:
            Score += 5000;
            FloatSomeText(X, Y, "5000");
            break;
            
        case Pow:
            if (Power != 2) Power++;
            FloatSomeText(X, Y, "Power up!", {50, 50, 255});
            break;
    };
    
    X = -100;
}

bool Pickup::IsDead()
{
    return (X < -21);
}

void CreatePickup(float X, float Y, PickupType TypeEnum)
{
    Pickup PushMe(X, Y, TypeEnum);
    PickupVector.push_back(PushMe);
}

void DoPickups()
{
    for (int i = 0; i < PickupVector.size(); i++)
        PickupVector.at(i).Do();
    
    PickupVector.erase(
        std::remove_if(
        PickupVector.begin(),
        PickupVector.end(),
        std::mem_fun_ref((&Pickup::IsDead))),
        PickupVector.end());
}