#ifndef RESOLVECOLLISIONS_H
#define	RESOLVECOLLISIONS_H

#include "FloatText.h"
#include "Enemies.h"
#include "Projectiles.h"
#include "EnemyProjectiles.h"
#include "Pickups.h"

bool ResolveCollisions(SDL_Rect PlayerRect, Sprite PlayerSprite);

#endif