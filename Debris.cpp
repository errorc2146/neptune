#include "Debris.h"

std::vector <Debris> DebrisVector;

Debris::Debris(SDL_Rect DebrisRect, float IXVel, float IYVel, int ITime, Uint32 IColour)
{
    X = DebrisRect.x;
    Y = DebrisRect.y;
    W = DebrisRect.w;
    H = DebrisRect.h;
    XVel = IXVel;
    YVel = IYVel;
    Time = ITime;
    Colour = IColour;
}

void Debris::Do()
{
    SDL_Rect RenderRect = {X, Y, W, H};
    
    if (!IsIntersecting(RenderRect, ScreenRect)) Time = 0;
    
    if (Time != 0)
    {
        Time--;
        X += XVel;
        Y += YVel;
    
        SDL_FillRect(Screen, &RenderRect, Colour);
    }
}

bool Debris::OutOfTime()
{
    return (Time == 0);
}

void CreateDebris(SDL_Rect DebrisRect, float XVel, float YVel, int Time, Uint32 Colour)
{
    Debris PushMe(DebrisRect, XVel, YVel, Time, Colour);
    DebrisVector.push_back(PushMe);
}

void DoDebris()
{
    for (int i = 0; i < DebrisVector.size(); i++)
        DebrisVector.at(i).Do();
    
    DebrisVector.erase(
        std::remove_if(
        DebrisVector.begin(),
        DebrisVector.end(),
        std::mem_fun_ref((&Debris::OutOfTime))),
        DebrisVector.end());
}