#ifndef ENEMYPROJECTILES_H
#define	ENEMYPROJECTILES_H

#include "Globals.h"

enum EnemyProjectileEnum {StdProj, Slow1};

class EnemyProjectile
{
private:
    float X,Y;
    int W,H;
    float XVel, YVel;
    EnemyProjectileEnum TypeEnum;
    Uint32 Colour;
    int  Time;
    bool Dead;
    
public:
    EnemyProjectile(float x, float Y, EnemyProjectileEnum Type, float Bearing, float Speed);
    SDL_Rect GetCollisionRect();
    void Destroy();
    bool IsDead();
    void Do();
};

extern std::vector <EnemyProjectile> EnemyProjectileVector;

void CreateEnemyProjectile(float X, float Y, EnemyProjectileEnum Type, float Bearing = 0, float Speed = 0);
void DoEnemyProjectiles();

#endif

