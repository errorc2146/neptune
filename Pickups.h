#ifndef PICKUPS_H
#define	PICKUPS_H

#include "Globals.h"
#include "Sprite.h"
#include "FloatText.h"

enum PickupType {Life, Points1, Points2, Points3, Pow};

class Pickup
{
private:
    float X,Y;
    float XVel, YVel;
    PickupType Type;
    Sprite CurrentSprite;
    
public:
    Pickup(float x, float y, PickupType TypeEnum);
    SDL_Rect GetCollisionRect(); //I wish I used inheritence
    void Do();
    void Destroy();
    bool IsDead();
};

extern std::vector <Pickup> PickupVector;

void CreatePickup(float X, float Y, PickupType TypeEnum);
void DoPickups();

#endif

