#ifndef SPRITES_H
#define	SPRITES_H

#define NUMBEROFPICKUPS 5
#define NUMBEROFENEMIES 6

extern short unsigned int PlayerFrames[];

//Enemies
extern short unsigned int GruntFrames[2][25];
extern short unsigned int ClapperFrames[2][24];
extern short unsigned int SpinnerFrames[2][36];
extern short unsigned int WeirdFrames[2][21];
extern short unsigned int BirdFrames[2][15];
extern short unsigned int DispensorFrames[2][25];
extern short unsigned int SpammerFrames[2][36];
extern short unsigned int AngstFrames[2][49];

extern short unsigned int AsteroidBig[225];
extern short unsigned int AsteroidMed[121];
extern short unsigned int AsteroidSmall[49];

//Pickups
extern short unsigned int *PickupSpriteArray[NUMBEROFPICKUPS];

#endif
