#ifndef GAME_H
#define	GAME_H

#include "Globals.h"
#include "Sprite.h"
#include "Sprites.h"
#include "Projectiles.h"
#include "Enemies.h"
#include "Projectiles.h"
#include "EnemyPatterns.h"
#include "ResolveCollisions.h"
#include "LevelPatterns.h"

void Game();

#endif

