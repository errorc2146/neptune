#include "ResolveCollisions.h"
#include "EnemyProjectiles.h"

#define CURRENTPROJECTILE ProjectileVector.at(i)
#define CURRENTENEMY EnemyVector.at(j)
#define CURRENTENEMYPROJECTILE EnemyProjectileVector.at(i)
#define CURRENTPICKUP PickupVector.at(i)

bool ResolveCollisions(SDL_Rect PlayerRect, Sprite PlayerSprite)
{ 
    bool PlayerDamaged = false;
    
    //Enemies and player/player projectiles
    for (int j = 0; j < EnemyVector.size(); j++)
    {
        if (CURRENTENEMY.IsColliding(PlayerRect)) PlayerDamaged = true;
        
        for (int i = 0; i < ProjectileVector.size(); i++)
        {
            if (CURRENTENEMY.IsColliding(CURRENTPROJECTILE.GetCollisionRect()))
            {
                CURRENTENEMY.Damage(10);
                CURRENTPROJECTILE.Destroy();
            }
        }
    }
    
    //Player and enemy projectiles
    for (int i = 0; i < EnemyProjectileVector.size(); i++)
    {    
        if (IsIntersecting(PlayerRect, CURRENTENEMYPROJECTILE.GetCollisionRect()) 
            && PlayerSprite.IsColliding(PlayerRect.x, PlayerRect.y, CURRENTENEMYPROJECTILE.GetCollisionRect()))
        {
            PlayerDamaged = true;
            CURRENTENEMYPROJECTILE.Destroy();
        }
    }
    
    //Player and pickups
    for (int i = 0; i < PickupVector.size(); i++)
    {    
        if (IsIntersecting(PlayerRect, CURRENTPICKUP.GetCollisionRect()))
            CURRENTPICKUP.Destroy();
    }
    
    return PlayerDamaged;
}