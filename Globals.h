#ifndef GLOBALS_H
#define	GLOBALS_H

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <functional>
#include <time.h>
#include <string>
#include <sstream>
#include <iomanip>
#include "GetXYRatio.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"

#define SCREENWIDTH 1920
#define SCREENHEIGHT 1080

extern SDL_Surface *Screen;

extern SDL_Colour White;

extern SDL_Rect ScreenRect;

extern TTF_Font *BigFont;
extern TTF_Font *MedFont;
extern TTF_Font *SmallFont;
extern TTF_Font *RealSmallFont;

extern std::stringstream SpareStream;

extern int Score;
extern int Lives;
extern int Power;

bool Init();
bool MakeBetween(float Min, float *Value, float Max);
bool InBetween(float Min, float Value, float Max);
bool IsIntersecting(SDL_Rect Rect1, SDL_Rect Rect2);
void ApplySurface(int X, int Y, SDL_Surface *Source, SDL_Surface *Destination = Screen);

#endif