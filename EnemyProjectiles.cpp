#include "EnemyProjectiles.h"

std::vector <EnemyProjectile> EnemyProjectileVector;

EnemyProjectile::EnemyProjectile(float x, float y, EnemyProjectileEnum Type, float Bearing, float Speed)
{
    X = x;
    Y = y;
    TypeEnum = Type;
    Dead = false;
    
    switch (TypeEnum)
    {
        case StdProj:
            W = 30;
            H = 4;
            XVel = -15;
            YVel = 0;
            Colour = 0xFC0505;
            break;
            
        case Slow1:
            W = 15;
            H = 15;
            Colour = 0xFF0505;
            if (Speed == 0) Speed = 8;
            GetXYRatio(&XVel, &YVel, Bearing, Speed);
            break;
    }
}

SDL_Rect EnemyProjectile::GetCollisionRect()
{
    return {X,Y,W,H};
}

void EnemyProjectile::Destroy()
{
    Dead = true;
}

bool EnemyProjectile::IsDead()
{
    return (Dead || X < -W);
}

void EnemyProjectile::Do()
{
    X += XVel;
    Y += YVel;
    SDL_Rect FillMe = GetCollisionRect();
    SDL_FillRect(Screen, &FillMe, Colour);
}

void CreateEnemyProjectile(float X, float Y, EnemyProjectileEnum Type, float Bearing, float Speed)
{
    EnemyProjectile PushMe(X, Y, Type, Bearing, Speed);
    EnemyProjectileVector.push_back(PushMe);
}

void DoEnemyProjectiles()
{
    for (int i = 0; i < EnemyProjectileVector.size(); i++)
        EnemyProjectileVector.at(i).Do();
    
    EnemyProjectileVector.erase(
        std::remove_if(
        EnemyProjectileVector.begin(),
        EnemyProjectileVector.end(),
        std::mem_fun_ref((&EnemyProjectile::IsDead))),
        EnemyProjectileVector.end());
}