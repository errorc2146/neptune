#ifndef SPRITE_H
#define	SPRITE_H

#include "Globals.h"
#include "Sprites.h"
#include "Debris.h"

class Sprite
{
private:
    int W, H;
    int FragmentSize;
    Uint32 Colour;
    short unsigned int *SpriteData;
    
public:
    Sprite(short unsigned int *Sprite = NULL, int Width = 0, int Height = 0, 
           Uint32 IColour = 0xFFFFFF, int FragSize = 6);
    bool IsColliding(int X, int Y, SDL_Rect Rect);
    void Render(int X, int Y, Uint32 OverrideColour = 0xC0FFEE, int OverrideFragSize = 0);
    void Update(short unsigned int *Sprite, int Width = 0, int Height = 0, 
                Uint32 IColour = 0xC0FFEE, int FragSize = 6);
    void Vaporise(int X, int Y, float XVel = 0, float YVel = 0);
};

#endif
