#ifndef DEBRIS_H
#define	DEBRIS_H

#include "Globals.h"

class Debris
{
private:
    int Time;
    int W,H;
    float X,Y;
    float XVel, YVel;
    Uint32 Colour;
    
public:
    Debris(SDL_Rect DebrisRect, float IXVel, float IYVel, int ITime, Uint32 IColour);
    void Do();
    bool OutOfTime();
};

extern std::vector <Debris> DebrisVector;

void CreateDebris(SDL_Rect DebrisRect, float XVel, float YVel, int Time = -1, Uint32 Colour = 0xF0F0F0);
void DoDebris();

#endif

