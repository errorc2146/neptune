#ifndef ENEMYPATTERNS_H
#define	ENEMYPATTERNS_H

#include "Enemies.h"

enum EnemyPattern {Line, Dart, VertLine, Single};

class RunningPattern
{
private:
    float X,Y;
    int Delay, Time, Number;
    int Spare;
    EnemyPattern Type;
    EnemyType SpawnMe;
    
public:
    RunningPattern(EnemyPattern TypeEnum, EnemyType EnemyEnum, float x,  float y,
                   int Parameter, int Num, int Del);
    void Run();
    bool IsFinished();
};

extern std::vector <RunningPattern> PatternVector;

void RunPattern(EnemyPattern PatternEnum, EnemyType EnemyEnum, float X = 0, 
                float Y = 0, int Spare = 0, int Number = 0, int Delay = 0);
void CheckPatterns();

#endif

